module VMCFG
	# ID задачи
	ID = "240322"

	# определяем подсеть
	SUBNET = "192.168.223."

	# проверять обновления репозитория?
	VM_BOX_CHECK_UPDATE = false

	# Использовать назначенные скрипты? Если =false, то скрипты в VM_CONFIG игнорируются
	SCRIPTS_USE = true

	# создаем список нужных виртуалок
	VM_CONFIG = [
	  { # имя ВМ 1
	    :vm_name => "dev01",
	    # образ системы Ubuntu 
	    :box => "ubuntu/focal64",
	    # настройки ВМ
		:vbox_config => [
	        { "--cpus" => "1" },
	        { "--memory" => "2048" },
	        { "--description" => "Host dev01"}
	    ],

	    :script => [
	    	"./files/scripts/Docker.sh",
	    	"./files/scripts/prep_dev.sh"
	    ]
	  },
	  { # имя ВМ 2
	   :vm_name => "prod01",
	   :box => "ubuntu/focal64",
	   :vbox_config => [
	        { "--cpus" => "1" },
	        { "--memory" => "2048" },
	        { "--description" => "Host prod01"}
	    ],

	    :script => [
	    	"./files/scripts/Docker.sh",
	    	"./files/scripts/MySQL.sh"
	    ]
	  }

	]
end
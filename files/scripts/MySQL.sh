#!/bin/bash
# Подключаем переменные из файла vars
. /vagrant/files/scripts/vars
# Установка MySQL
sudo apt update
sudo apt install -y mysql-server
# Cоздание файла настроек по шагам "sudo mysql_secure_installation"
tempSqlFile='tempScript.sql'
echo 'FLUSH PRIVILEGES;' > $tempSqlFile
echo "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$MYSQL_ROOT_PASSWORD';" >> $tempSqlFile
echo "CREATE DATABASE $MYSQL_DB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;" >> $tempSqlFile
echo "CREATE USER '$MYSQL_USER'@'%' IDENTIFIED BY '$MYSQL_PASSWORD';" >> $tempSqlFile
echo "GRANT ALL PRIVILEGES ON $MYSQL_DB.* TO '$MYSQL_USER'@'%';" >> $tempSqlFile
echo 'DROP USER IF EXISTS "";' >> $tempSqlFile
echo 'DELETE FROM mysql.user WHERE User = "";' >> $tempSqlFile
echo 'DELETE FROM mysql.user WHERE User = "root" AND Host NOT IN ("localhost", "127.0.0.1", "::1");' >> $tempSqlFile
echo 'DROP DATABASE IF EXISTS test;' >> $tempSqlFile
echo 'DELETE FROM mysql.db WHERE Db = "test" OR Db = "test\_%";' >> $tempSqlFile
echo 'FLUSH PRIVILEGES;' >> $tempSqlFile
# Выполняем скрипт
sudo mysql < $tempSqlFile
# Удаляем временный файл
rm -f $tempSqlFile
# Меняем настройки MySQL для удалённого подключения к БД
sudo sed -i 's/^bind-address/# &/' /etc/mysql/mysql.conf.d/mysqld.cnf
sudo sed -i 's/^mysqlx-bind-address/# &/' /etc/mysql/mysql.conf.d/mysqld.cnf
# Ребутим службу
sudo systemctl restart mysql
# Мы молодцы
echo "MySQL user & database created."
IPDEV01="127.0.0.1"
git clone https://gitlab.deusops.com/deuslearn/classbook/todo-list-app.git /usr/share/app
cd /usr/share/app
cp /vagrant/files/Dockerfile ./
cp /vagrant/files/docker-compose.yml ./
cp /vagrant/files/.dockerignore ./
cp /vagrant/files/.env_app ./
cp /vagrant/files/.env_db ./
sudo sh -c "echo $IPDEV01 dev.example.com >> /etc/hosts"
docker compose up -d --build
#!/bin/bash
# Подключаем переменные из файла vars
. /vagrant/files/scripts/vars
# Определяем IP адрес хоста. Используем в качетсве имя БД
IPPROD01=$(ip addr show docker0 | awk '$1 == "inet" {gsub(/\/.*$/, "", $2); print $2}')
# Скачиваем приложение
git clone https://gitlab.deusops.com/deuslearn/classbook/todo-list-app.git ~/app
cd ~/app
cp /vagrant/files/Dockerfile ./
# Добавляем DNS запись в файл hosts
sudo sh -c "echo $IPPROD01 example.com >> /etc/hosts"
# Собираем образ
docker build -t app:latest .
# Запускаем контейнер из образа с нужными нам параметрами
docker run -d -p $DOCKER_PORT:3000 -e MYSQL_HOST=$IPPROD01 -e MYSQL_USER=$MYSQL_USER -e MYSQL_PASSWORD=$MYSQL_PASSWORD -e MYSQL_DB=$MYSQL_DB app:latest
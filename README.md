ID24322
========

Light:
------
1. Vagrantfile
2. ./files/scripts/Docker.sh
3. ./files/Dockerfile
4. ./files/docker-compose.yml
   (tag v1.4)
5. Добавлены скрипты для автоматической настройки окружения prod01 при раскатке через vagrant (прописаны в vagrant_vars.rb). Скрипты используют переменные из файла ./files/scripts/vars:
   - MySQL.sh     - установка и настройка MySQL;
   - Docker.sh    - установка Docker;
   - prep_prod.sh - скачиваем приложение. Создаём докер-образ, запускаем контейнер с приложением
   (tag v1.5)

Normal:
------
1. Проект https://gitlab.com/deusops7662802/ID240322-simle-todo-app.git
2. файл .gitlab-ci.yml
   (tag v2.2)
3. файл .gitlab-ci.yml
   (tag v2.3)
4. файл .gitlab-ci.yml
   (tag v2.4)
5. файл .gitlab-ci.yml
   (tag v2.5)
6. файл .gitlab-ci.yml
   (tag v2.6)

Hard:
------
1. файл .gitlab-ci.yml
   (tag v3.1)
2. файл .gitlab-ci.yml
   (tag v3.2)
3. файл .gitlab-ci.yml
   (tag v3.3)
4. файл .gitlab-ci.yml
   (tag v3.4)